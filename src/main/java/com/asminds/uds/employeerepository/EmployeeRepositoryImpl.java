package com.asminds.uds.employeerepository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.asminds.uds.employeemodel.EmployeeModel;
@Repository
public class EmployeeRepositoryImpl {
private SessionFactory sessionFactory;
	
	private Session getSession(){
		return sessionFactory.getCurrentSession();
	}
    @Autowired
	public EmployeeRepositoryImpl(SessionFactory sessionFactory) {
		super();
	    this.sessionFactory=sessionFactory;
	}
    @SuppressWarnings({ "unchecked", "deprecation" })
	public List<EmployeeModel> getAll() {
		return getSession().createQuery("from EmployeeModel").list();
	}

	public EmployeeModel add(EmployeeModel emp) {
		System.out.println("fghj");
		getSession().save(emp);
		System.out.println(emp);
		return emp;
	}



	public EmployeeModel update(EmployeeModel emp) {
		getSession().update(emp);
		return emp;
	}

	
	
	
	

}
