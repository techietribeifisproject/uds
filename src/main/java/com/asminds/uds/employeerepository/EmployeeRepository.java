package com.asminds.uds.employeerepository;

import java.util.List;


import com.asminds.uds.employeemodel.EmployeeModel;

public interface EmployeeRepository  {
	public List<EmployeeModel> getAll();
	public EmployeeModel add(EmployeeModel emp);
	
	public EmployeeModel update(EmployeeModel employeeid);



}
