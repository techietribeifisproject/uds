package com.asminds.uds.employeeservice;

import java.util.List;

import org.springframework.stereotype.Component;

import com.asminds.uds.employeemodel.EmployeeModel;
@Component
public interface EmployeeService {
	public List<EmployeeModel> getAll();
	public EmployeeModel add(EmployeeModel emp);
	
	public EmployeeModel update(EmployeeModel employeeid);

}
