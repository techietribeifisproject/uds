package com.asminds.uds.employeeservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asminds.uds.employeemodel.EmployeeModel;
import com.asminds.uds.employeerepository.EmployeeRepositoryImpl;

@Service
@Transactional
public class EmployeeServiceImpl  implements EmployeeService{
	
	
	private EmployeeRepositoryImpl emp1;
	
    @Autowired
	public EmployeeServiceImpl(EmployeeRepositoryImpl emp1) {
		this.emp1=emp1;
	}
    
	public List<EmployeeModel> getAll() {
		
		return emp1.getAll();
	}

	public EmployeeModel add(EmployeeModel emp) {
		System.out.println("i am in service");
		return emp1.add(emp);
	}

	public EmployeeModel update(EmployeeModel emp) {
		
		return emp1.update(emp);
	}
	

}
