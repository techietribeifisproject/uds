package com.asminds.uds.employeemodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EmployeeModel {
	@Id
	@GeneratedValue
	private int employeeid;
	
	private String firstname;
	private String lastname;
	private long phonenumber;
	private String designation;
	private String email;
	private String userrole;
	public int getEmployeeid() {
		return employeeid;
	}
	public void setEmployeeid(int employeeid) {
		this.employeeid = employeeid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public long getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(long phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserrole() {
		return userrole;
	}
	public void setUserrole(String userrole) {
		this.userrole = userrole;
	}
	public EmployeeModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public EmployeeModel(int employeeid, String firstname, String lastname, long phonenumber, String designation,
			String email, String userrole) {
		super();
		this.employeeid = employeeid;
		this.firstname = firstname;
		this.lastname = lastname;
		this.phonenumber = phonenumber;
		this.designation = designation;
		this.email = email;
		this.userrole = userrole;
	}
	@Override
	public String toString() {
		return "EmployeeModel [employeeid=" + employeeid + ", firstname=" + firstname + ", Lastname=" + lastname
				+ ", phonenumber=" + phonenumber + ", designation=" + designation + ", email=" + email + ", userrole="
				+ userrole + "]";
	}
	
	
	
	

}
