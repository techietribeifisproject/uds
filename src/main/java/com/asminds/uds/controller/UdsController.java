package com.asminds.uds.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.asminds.uds.employeemodel.EmployeeModel;
import com.asminds.uds.employeeservice.EmployeeService;

@Controller
public class UdsController {
	
	@Autowired
	EmployeeService es;
	
	
	@RequestMapping("/index")
	public String index() {
		System.out.println("im index");
		return "index";
	}
	@RequestMapping("/employee")
	public String employee() {
		System.out.println("im index");
		return "empreg";
	}
	@RequestMapping("/client")
	public String client() {
		System.out.println("im index");
		return "client";
	}
	@RequestMapping("/site")
	public String site() {
		System.out.println("im index");
		return "site";
	}
	@RequestMapping("/jobs")
	public String jobs() {
		System.out.println("im index");
		return "jobs";
	}
	@RequestMapping("/ticket")
	public String ticket() {
		System.out.println("im index");
		return "ticket";
	}
	@RequestMapping("/save")
	public String save(@ModelAttribute("t") EmployeeModel emp)
	{
		System.out.println("I am in save page");
		es.add(emp);
		return "redirect:/index";
		
	}
	
	

}
