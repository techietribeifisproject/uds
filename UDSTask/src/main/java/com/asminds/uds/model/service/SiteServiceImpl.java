package com.asminds.uds.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asminds.uds.model.Site;
import com.asminds.uds.model.repository.SiteRepository;

@Service
@Transactional
public class SiteServiceImpl implements SiteService {
	
	
	SiteRepository repo;
	
	public SiteServiceImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public SiteServiceImpl(SiteRepository repo)
	{
		this.repo = repo;
	}
	
	public Site add(Site s) {
		
		return repo.add(s);
	}

	public Site delete(Site s) {
	
		return repo.delete(s);
	}

	public List<Site> view() {
		
		return repo.view();
	}

	/*
	 * public Site update(Site s) {
	 * 
	 * return repo.update(s); }
	 */
}
