package com.asminds.uds.model.repository;

import java.util.List;

import com.asminds.uds.model.Site;



public interface SiteRepository {
	
	
		public Site add(Site s);
		public Site delete(Site s);
		public List<Site> view();
	/* public Site update(Site s); */ 
	
		
		
}