package com.asminds.uds.model.repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import org.springframework.stereotype.Repository;

import com.asminds.uds.model.Site;

@Repository

public class SiteRepoImpl implements SiteRepository{

private SessionFactory sf;
	
	private Session getSession() {
		return sf.getCurrentSession();
	}
	
	public SiteRepoImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public SiteRepoImpl(SessionFactory sf) {
		super();
		this.sf = sf;
	}
	
	public Site add(Site s) {
		getSession().save(s);
		return s;
	}

	public Site delete(Site s) {
		if(s!=null)
			getSession().delete(s);
		return s;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Site> view() {
		
		return getSession().createQuery("from Site").list();
	}

	/*
	 * public Site update(Site s) { getSession().saveOrUpdate(s); return s; }
	 */
	
	
}
