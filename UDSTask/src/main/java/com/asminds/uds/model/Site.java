package com.asminds.uds.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Site {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String clientname;
	private String region;
	private String branch;
	private String sitename;
	private String city;
	private String state;
	private String pincode;
	public String getClientname() {
		return clientname;
	}
	public void setClientname(String clientname) {
		this.clientname = clientname;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getSitename() {
		return sitename;
	}
	public void setSitename(String sitename) {
		this.sitename = sitename;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public Site(String clientname, String region, String branch, String sitename, String city, String state,
			String pincode) {
		super();
		this.clientname = clientname;
		this.region = region;
		this.branch = branch;
		this.sitename = sitename;
		this.city = city;
		this.state = state;
		this.pincode = pincode;
	}
	public Site() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
